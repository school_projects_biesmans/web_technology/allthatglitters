let categoryArray = [
    ["Necklaces", "A selection of handmade necklaces and pendants for you to choose from. Uniquely made for you with or without gemstones.", "necklaces"],
    ["Rings", "Rings to suit all tastes and budgets. From sterling silver rings with gemstones to more simple, elegant designs. Find a hidden gem to suit you in our treasure trove of handmade rings.", "rings"],
    ["Ear jewellery", "Earrings can transform your look. We have earrings to suit every taste, occasion and budget, from fun earrings for a night out to stunning sterling silver earrings with precious stones that you could wear on your wedding day.", "ear_jewellery"],
    ["Bracelets", "Beautiful handmade bracelets and bangles. Find a unique bracelet or bangle in this collection, the perfect way to accessorise any new outfit or a great way to update your look.", "bracelets"]
];

let productArray = [
    ["Necklaces", "Pineapple Almond", "neck_01.jpg", "Gemstone: Swarovski Crystal", "pineapple"],
    ["Necklaces", "Butterfly", "neck_02.jpg", "Gemstone: Blue Chalcedony", "butterfly"],
    ["Necklaces", "Eye Am Watching You", "neck_03.jpg", "Gemstone: Amethyst", "eye"],
    ["Necklaces", "Two Rings", "neck_04.jpg", "Naked: silver, leather", "two_rings"],
    ["Necklaces", "Moon Paradise", "neck_05.jpg", "Gemstone: Swarovski Crystal", "moon_paradise"],
    ["Necklaces", "Bird Skull", "neck_06.jpg", "Naked: bronze, silver", "bird_skull"],
    ["Necklaces", "June", "neck_07.jpg", "Gemstone: Moonstone", "no_link"],
    ["Necklaces", "Woodland necklace", "neck_08.jpg", "Gemstone: Pearl", "no_link"],
    ["Rings", "Scribble ring", "ring_01.jpg", "Gemstone: Clear Quartz", "no_link"],
    ["Rings", "Clear Quartz ring", "ring_02.jpg", "Gemstone: Clear Quartz", "no_link"],
    ["Rings", "Moonstone ring", "ring_03.jpg", "Gemstone: Moonstone", "no_link"],
    ["Rings", "Leaf", "ring_04.jpg", "Naked: silver", "no_link"],
    ["Rings", "Salamander ring", "ring_05.jpg", "Gemstone: Pearl", "no_link"],
    ["Rings", "Abstract ring", "ring_06.jpg", "Naked: silver", "no_link"],
    ["Rings", "Twig and Peridot", "ring_07.jpg", "Gemstone: Peridot", "no_link"],
    ["Ear jewellery", "Celtic ear cuff", "ear_01.jpg", "Naked: silver", "no_link"],
    ["Ear jewellery", "Pearl earrings", "ear_02.jpg", "Gemstone: Pearl", "no_link"],
    ["Ear jewellery", "The silver eye", "ear_03.jpg", "Naked: silver", "no_link"],
    ["Ear jewellery", "Silver ear cuff", "ear_04.jpg", "Naked: silver", "no_link"],
    ["Ear jewellery", "Small pearl earrings", "ear_05.jpg", "Gemstone: Pearl", "no_link"],
    ["Ear jewellery", "Egyptian earrings", "ear_06.jpg", "Naked: silver", "no_link"],
    ["Ear jewellery", "Wire wrapped earrings", "ear_07.jpg", "Gemstone: Moonstone", "no_link"],
    ["Ear jewellery", "Moonstone ear cuff", "ear_08.jpg", "Gemstone: Moonstone", "no_link"],
    ["Bracelets", "Egyptian bracelet", "arm_01.jpg", "Naked: silver", "no_link"],
    ["Bracelets", "Double bird skull", "arm_02.jpg", "Naked: silver", "no_link"],
    ["Bracelets", "Lotus flower", "arm_03.jpg", "Gemstone: Pearl", "no_link"],
    ["Bracelets", "Swirl", "arm_04.jpg", "Gemstone: Moonstone", "no_link"]
];