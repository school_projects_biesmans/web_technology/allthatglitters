addEventListener("load", init);

function init() {
    addContactInfo();
    addMaterialAndCategory();
    addGemstone("moonstone");
    addGemstone("iolite");
    addGemstone("amethyst");
    addGemstone("pearl");
    addCrystal();
    addInitials();
    addRemarks();
}

function addContactInfo() {
    let name = document.querySelector("#conf-name");
    name.innerHTML = getParam("name");
    let email = document.querySelector("#conf-email");
    email.innerHTML = getParam("email");
}

function addMaterialAndCategory() {
    let matCat = document.querySelector("#conf-material-category");
    let material = getParam("material");
    let category = getParam("category");
    matCat.innerHTML = material.charAt(0).toUpperCase() + material.slice(1) + " " + category.charAt(0).toUpperCase() + category.slice(1);
}

function addGemstone(gemstone) {
    if ((getParam(gemstone) !== "0") && (getParam(gemstone) !== "")) {
        let gem = document.querySelector("#conf-" + gemstone);
        gem.innerHTML = gemstone.charAt(0).toUpperCase() + gemstone.slice(1) + ": " + getParam(gemstone) + " pcs";
    }
}

function addCrystal() {
    if ((getParam("crystal") !== "0") && (getParam("crystal") !== "")) {
        let crystal = document.querySelector("#conf-crystal");
        crystal.innerHTML = "Swarovski Crystal: " + getParam("crystal") + " pcs";

        let canvas = document.querySelector("#conf-crystal-color");
        canvas.removeAttribute("class");    // the canvas is no longer hidden
        let square = canvas.getContext("2d");
        square.beginPath();
        square.rect(0, 0, 30, 30);
        square.fillStyle = getParam("ccolor");
        square.fill();
    }
}

function addInitials() {
    if ((getParam("initials") !== "") && (getParam("txtinitials") !== "")) {
        let initials = document.querySelector("#conf-initials");
        initials.innerHTML = "Initials: " + getParam("txtinitials");
    }
}

function addRemarks() {
    if (getParam("remarks") !== "") {
        let remarks = document.querySelector("#conf-extra-info");
        remarks.innerHTML = getParam("remarks");
    }
}

function getParam(name) {
    let queryString = decodeURIComponent(window.location.search.slice(1));
    let params = queryString.split("&");
    let value = "";
    for (let i = 0; i < params.length; i++) {
        let parts = params[i].split("=");
        if (parts[0] === name) {
            value = parts[1];
            break;
        }
    }
    return value.replace(/\+/g, " ");    // + to space
}