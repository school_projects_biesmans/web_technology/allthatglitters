window.addEventListener("load", init)

class Product {
    constructor(category, title, img, gem, link) {
        this.category = category;
        this.title = title;
        this.img = img;
        this.gem = gem;
        this.link = link;
    }

    getH4Tag() {
        let productTitle = document.createElement("h4");
        let productTitleTextNode = document.createTextNode(this.title);
        productTitle.appendChild(productTitleTextNode);
        return productTitle;
    }

    // check if a link is present and return the correct tags
    getImgOrLinkTag() {
        if (this.link === "no_link") {
            return this.getImgTag();
        } else {
            return this.getImgLinkTag();
        }
    }

    getImgLinkTag() {
        let productLink = document.createElement("a");
        productLink.setAttribute("href", "necklaces/" + this.link + ".html");
        productLink.appendChild(this.getImgTag());
        return productLink;
    }

    getImgTag() {
        let productImg = document.createElement("img");
        const productImgSrc = "../media/img/product/thumb/";
        const productImgDim = "300";
        productImg.setAttribute("src", productImgSrc + this.img);
        productImg.setAttribute("width", productImgDim);
        productImg.setAttribute("height", productImgDim);
        productImg.setAttribute("alt", this.title);
        return productImg;
    }

    getPTag() {
        let productGem = document.createElement("p");
        let productGemTextNode = document.createTextNode(this.gem);
        productGem.appendChild(productGemTextNode);
        return productGem;
    }
}

class ProductCategory {
    constructor(name, description, sectionId) {
        this.name = name;
        this.description = description;
        this.sectionId = sectionId;
    }

    getName() {
        return this.name;
    }

    getH3Tag() {
        let categoryTitle = document.createElement("h3");
        let categoryTitleTextNode = document.createTextNode(this.name);
        categoryTitle.setAttribute("id", this.sectionId);
        categoryTitle.appendChild(categoryTitleTextNode);
        return categoryTitle;
    }

    getPTag() {
        let categoryDescription = document.createElement("p");
        let categoryDescriptionTextNode = document.createTextNode(this.description);
        categoryDescription.appendChild(categoryDescriptionTextNode);
        return categoryDescription;
    }
}

function init() {
    addStaticPageLink();
    buildMain();
    adjustSubmenu();
}

function adjustSubmenu() {
    // add id's to the submenu for the internal links to work
    let submenuLinks = document.querySelectorAll("li.submenu a");
    let submenuNames = ["necklaces", "rings", "ear_jewellery", "bracelets"];
    for (let i = 0; i < submenuNames.length; i++) {
        let submenuLinkHref = "#" + submenuNames[i];
        submenuLinks[i + 1].setAttribute("href", submenuLinkHref);
    }
}

function addStaticPageLink() {
    let containerMain = document.querySelector(".product-main");

    // initialise html elements
    let staticAside = document.createElement("aside");
    let staticTitle = document.createElement("h2");
    let staticLink = document.createElement("a");

    // aside element
    let staticAsideClass = "dynamic-page-link";
    staticAside.setAttribute("class", staticAsideClass);

    // h2 element
    let staticTitleClass = "hide";
    staticTitle.setAttribute("class", staticTitleClass);

    let staticTitleText = "Static page";
    let staticTitleTextNode = document.createTextNode(staticTitleText);

    // a element
    let staticLinkHref = "product.html";
    staticLink.setAttribute("href", staticLinkHref);
    let staticLinkText = "Static";
    let staticLinkTextNode = document.createTextNode(staticLinkText);

    // append children
    staticTitle.appendChild(staticTitleTextNode);
    staticLink.appendChild(staticLinkTextNode);
    staticAside.appendChild(staticTitle);
    staticAside.appendChild(staticLink);
    addReverseButton(staticAside);
    containerMain.appendChild(staticAside);
}

function addReverseButton(containerAside) {
    let button = document.createElement("button");
    let buttonType = "button";
    let buttonStyle = "padding: 3px;margin: 1em 0;width: 200px;border-radius: 5px;font-size: 1.3em;background-color: #B78727;color: #001D3D;";
    let buttonText = "Reverse";
    button.setAttribute("type", buttonType);
    button.setAttribute("style", buttonStyle);
    let buttonTextNode = document.createTextNode(buttonText);
    button.appendChild(buttonTextNode);
    containerAside.appendChild(button);

    button.addEventListener("click", reverseContent);
}

function reverseContent() {
    // empty content of main
    let containerMain = document.querySelector(".product-main");
    containerMain.innerHTML = "";

    // reverse arrays
    categoryArray.reverse();
    productArray.reverse();

    // rebuild main
    addStaticPageLink();
    buildMain();
}

function buildMain() {
    let containerMain = document.querySelector(".product-main");
    let mainArticle = document.createElement("article");
    let mainTitle = document.createElement("h2");

    // h2 element
    let mainTitleText = "Jewellery";
    let mainTitleTextNode = document.createTextNode(mainTitleText);

    // append children
    mainTitle.appendChild(mainTitleTextNode);
    mainArticle.appendChild(mainTitle);
    addProductCategories(mainArticle);
    containerMain.appendChild(mainArticle);
}

function addProductCategories(containerArticle) {
    for (let i = 0; i < categoryArray.length; i++) {
        let productCategory = new ProductCategory(categoryArray[i][0], categoryArray[i][1], categoryArray[i][2]);
        addProductCategorySection(containerArticle, productCategory);
    }
}

function addProductCategorySection(containerArticle, productCategory) {
    let categoryHeader = document.createElement("header");
    categoryHeader.appendChild(productCategory.getH3Tag());
    categoryHeader.appendChild(productCategory.getPTag());

    let categorySection = document.createElement("section");
    categorySection.appendChild(categoryHeader);
    addProducts(productCategory.getName(), categorySection);
    containerArticle.appendChild(categorySection);
}

function addProducts(category, containerSectionCategory) {
    for (let i = 0; i < productArray.length; i++) {
        let product = new Product(productArray[i][0], productArray[i][1], productArray[i][2], productArray[i][3], productArray[i][4]);
        if (product.category === category) {
            addProductSection(containerSectionCategory, product);
        }
    }
}

function addProductSection(containerSectionCategory, product) {
    let productSection = document.createElement("section");
    productSection.appendChild(product.getH4Tag());
    productSection.appendChild(product.getImgOrLinkTag());
    productSection.appendChild(product.getPTag());
    containerSectionCategory.appendChild(productSection);
}