addEventListener("load", init);

function init() {
    changeFieldBgColors();
    let form = document.querySelector("form");
    document.querySelector("#name").addEventListener("keyup", validateName);
    document.querySelector("#email").addEventListener("blur", validateEmail);
    form.addEventListener("submit", validateForm);
}

function validateName() {
    let content = document.querySelector("#name").value;
    let feedback = document.querySelector("#error-name");
    if (content.length < 5) {
        feedback.style.color = "red";
        feedback.innerHTML = "Your input is smaller than 5 characters.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function validateEmail() {
    let content = document.querySelector("#email").value;
    let feedback = document.querySelector("#error-email");
    const pattern = /^(([a-z0-9_-])+\.([a-z0-9_-])+)+(@(student\.)?kdg\.be)$/;
    if (!pattern.test(content)) {
        feedback.style.color = "red";
        feedback.innerHTML = "Your entered e-mail does not belong to the KdG domain.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function contentIsEmpty() {
    let contentName = document.querySelector("#name").value;
    let contentEmail = document.querySelector("#email").value;
    return (contentName.empty() || contentEmail.empty());
}

function validateForm(event) {
    let nameOK = validateName();
    let emailOK = validateEmail();
    let feedback = document.querySelector("#error-submit");
    if (!nameOK || !emailOK || contentIsEmpty()) {
        feedback.style.color = "red";
        feedback.innerHTML = "Please make sure all fields are filled in correctly.";
        event.preventDefault();
    } else {
        feedback.innerHTML = "";
    }
}

function changeFieldBgColors() {
    let nameField = document.querySelector("#name");
    nameField.style.backgroundColor = "yellow";
    let emailField = document.querySelector("#email");
    emailField.style.backgroundColor = "orange";
}